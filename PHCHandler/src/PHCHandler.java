
import java.util.Scanner;


public class PHCHandler {
	//Scanner allescann = new Scanner(System.in);
	public static void main(String[] args) {
		double nettogesamtpreis;
		double bruttogesamtpreis;
		//String text;
		Scanner myScanner = new Scanner(System.in);		//Scanner einf�hren

		// Benutzereingaben lesen
		String artikelfrage =liesString ("Was moechten Sie bestellen?");	//Methode einen String �bergeben und ausgeben	

		int artikelzahl =liesInt ("Geben Sie die Anzahl ein:");

		double netto= liesDouble ("Geben Sie den Nettopreis ein:");
		

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		nettogesamtpreis = berechneGesamtnettopreis(artikelzahl, netto);
		bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis, mwst);
		

		// Ausgeben

		rechungausgeben(artikelfrage, artikelzahl, nettogesamtpreis, bruttogesamtpreis,mwst);
		myScanner.close();
	}
	public static String liesString(String text) {
		Scanner myScanner=new Scanner (System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;	
			
		
		
	}
	public static int liesInt(String text) {
		
		Scanner myScanner=new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;	
		
		
	}
	public static double liesDouble(String text) {
		Scanner myScanner=new Scanner(System.in);
		System.out.println(text);
		double netopreis = myScanner.nextDouble();
		return netopreis;	
		
		
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtnettopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
		
	}
	
}