import java.util.Random;
import java.util.Scanner;
//BWB �bung
//Autoren: Nick Schneider, Steffen Reuter, Dominik Jank, Gero Panzer und Maria Siegmann
//Sebtember 2020
public class Hangmann_simpel {

	public static void main(String[] args) throws InterruptedException {
		// w�rterliste
		String[] array = { "schildkr�te", "baumhaus", "monitor", "laserdrucker", "gegensprechanlage", "atomkraftwerk",
				"seppuku", "tastatureingabe", "grafikkarte", "befehlsliste", "kameralinse", "bruttosozialprodukt",
				"switch", "router", "fu�ball", "prozessor", "biene", "bl�mchiensex", "endoplasmatischesritikulum",
				"veltinsarena", "bier", "stier", "pirat", "pizza", "brennholzverleih", "styffen", "mitochondrien",
				"taco", "systemintegrator", "geh�usel�fter", "schlager", "quitscheentchen", "vortrag", "videospiel",
				"wasserflasche", "mundschutz", "kaffee", "zickermann", "easteregg", "hangman", "galgenm�nchen",
				"eclipse", "eierlegendewollmilchsau", "flugzeugtr�ger", "nagel", "quosigk", "kleas", "wassermelone" };
		String ram; // w�rter als char
		boolean frage = true; // spiel start oder ende
		int gewonnen = 0;

		do {
			Random rand = new Random(); // class Random hinzuf�gen
			int n = rand.nextInt(array.length); // l�nge der w�rterlist

			ram = array[n]; // das n`te element der liste wird ram zugewiesen
			char[] test = ram.toCharArray(); // ram in einhilfsarray des typs char speichern
			char[] hilfsarray = new char[test.length]; // hilfsarray hat die lenge von test

			System.out.printf("Das gesuchte Wort hat " + ram.length() + " Zeichen. \n");
			System.out.println("");
			for (int i = 0; i < ram.length(); i++) { // hilfsarray soll _ sein pro buchstabe
				System.out.print("_ ");
				hilfsarray[i] = '_';
			}
			System.out.println("\n");
			Scanner tastatur = new Scanner(System.in); // eingabe des spielers defininiert
			String eingabe;

			char c;
			int counter = 0;
			int hilfscounter = 0;
			int janein;
			String buchstaben=" ";
			
			while (test != hilfsarray || frage == true) { // schleife f�r buchstabe oder wort abfrage, startet wenn
															// frage=true ist
				
				System.out.println("\nGeben Sie einen kleinen Bustaben ein: ");
				eingabe = tastatur.next(); // eingabe des spielers
				buchstaben = buchstaben + eingabe;
				System.out.printf("Die bereits verwendeten Buchstaben: "+buchstaben +"\n");
				if (eingabe.equals(ram)) { // wenn eingabe wort den ausgew�hlen wort aus der liste enspricht ist das
											// spiel gewonnen
					
					
					System.out.println("Du hast gewonnen");
					System.out.println("Noch ne Runde? 1/0 ");
					janein = tastatur.nextInt(); // eingabe einf�hrung f�r frage ob weiter gespielt werden soll
					if (janein == 1) {
						frage = true; // wenn true wird weiter gespielt
					} else
						frage = false; // wenn false wird das program abgeprochen
					break;
				}

				if (eingabe.length() >= 2 && eingabe != ram) { // wenn ein word eingegeben wird unnd es falsch ist grigt
																// man ein fehlerpunkt
					counter += 1;
				} else { // ansonsten geht das program weiter
					c = eingabe.charAt(0); // das erste zeichen unserer eingabe wird auf c gespeichert
					hilfscounter = 0; // hilfscounter einf�hren und auf 0 setzen
					for (int j = 0; j < test.length; j++) { // es wird abgefragt ob c gleich eines buchstaben im
															// gesuchten wort ist
						if (test[j] == c) { // wenn enthalten
							hilfsarray[j] = c; // wird im hilfsarray erstzt
							hilfscounter += 1; // hilfscounter auf 1 setzen
						}
					}
					// fehler werden gez�hlt und ausgegeben
					if (hilfscounter == 0) {
						counter += 1;
						System.out.println(counter);
					}
				}
				// zeichnung
				switch (counter) {
				case 1: { // bei einen fehlerpunkt
					System.out.println("/");
					break;
				}
				case 2: {
					System.out.println("/ \\");
					break;
				}
				case 3: {
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println("/ \\");
					break;
				}
				case 4: {
					System.out.println(" _________");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println("/ \\");
					break;
				}
				case 5: {
					System.out.println(" _________");
					System.out.println(" |\t |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println("/ \\");
					break;
				}
				case 6: {
					System.out.println(" _________");
					System.out.println(" |\t |");
					System.out.println(" |\t o");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println(" |");
					System.out.println("/ \\");
					break;
				}
				case 7: {
					System.out.println(" _________");
					System.out.println(" |\t |");
					System.out.println(" |\t o");
					System.out.println(" |\t |");
					System.out.println(" |\t |");
					System.out.println(" |");
					System.out.println("/ \\");
					break;
				}
				case 8: {
					System.out.println(" _________");
					System.out.println(" |\t |");
					System.out.println(" |\t o");
					System.out.println(" |\t\\|/");
					System.out.println(" |\t |");
					System.out.println(" |");
					System.out.println("/ \\");
					break;
				}
				case 9: {
					System.out.println(" _________");
					System.out.println(" |\t |");
					System.out.println(" |\t o");
					System.out.println(" |\t\\|/");
					System.out.println(" |\t |");
					System.out.println(" |\t/");
					System.out.println("/ \\");
					break;
				}
				case 10: {
					System.out.println(" _________");
					System.out.println(" |\t |");
					System.out.println(" |\t o");
					System.out.println(" |\t\\|/");
					System.out.println(" |\t |");
					System.out.println(" |\t/ \\");
					System.out.println("/ \\");
					// wenn verloren die m�glichkeit geben noch mal zu spielen
					System.out.println("du hast verloren");
					System.out.println("Noch ne Runde? 1/0");
					janein = tastatur.nextInt();
					if (janein == 1) {
						frage = true;
					} else
						frage = false;
					break;
				}
				default: {
					break;
				}
				}
				// wenn jeden buchstaben einzelt erraten program beenden
				System.out.println(hilfsarray);
				for (int k = 0; k < test.length; k++) {
					gewonnen = 1;
					if (hilfsarray[k] == '_') {
						gewonnen = 0;
						break;
					}
				}
				// wenn ende frage ob noch mal spielen
				if (gewonnen == 1) {
					System.out.println("Du hast gewonnen");
					System.out.println("Noch ne Runde? 1/0 ");
					janein = tastatur.nextInt();
					if (janein == 1) {
						frage = true;
					} else
						frage = false;
					break;
				}
			}

		} while (frage == true); // wenn nicht noch mal spielen abschids gru�
		System.out.println("Vielen Dank f�rs Spielen!\nAuf Wiedersehen");
	}
}