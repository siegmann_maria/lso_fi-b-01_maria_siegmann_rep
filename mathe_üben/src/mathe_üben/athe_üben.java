package mathe_üben;

import java.util.Scanner;

public class athe_üben {
	
	public static void main(String[] args) {
		int i=1;		//Zähler für die while Schleife
		//Aufforderung die Anzahl der Kugel zu nennen
		//Anzahl ist ein int und wierd mit einen Scanner eingefühgt
		Scanner myscann= new Scanner (System.in);
		System.out.println("Für wie viele Kugeln möchten sie den Umfang, die Oberflöche und das Volumen berechnen lassen? ");
		int anzahl = myscann.nextInt();
		
		//while Schleife um ohne Neustart alle Kugeln abzuarbeiten
		while ( i <= anzahl) {
		Scanner zahl = new Scanner (System.in);
		System.out.println("\nWas ist der Radius ihrer Kugel in cm? ");
		double r = zahl.nextDouble();		//Radius der Kugel als doubel um alle möglichkeiten abzufangen
		//Ausgabe die aus den Methoden übernommen wird
		System.out.println("Umfang der Kugel: "+ umfang (r)+"cm");
		System.out.println ("Oberfläche der Kugel: "+ oberfläche (r)+"cm");
		System.out.println("Volumen der Kugel: "+volumen (r)+"cm");
		i++;	//Zähler nach oben zählen
		zahl.close();		//Scanner schließen
		}
		//Abschluss
		System.out.println("\nVielen Dank für die Nutzung von Kugelente");
		myscann.close();
		

		
	}
	//Methoden
	//Methode für den Umfang der Kugeln die ein doubel ausbigt und einen übernimt
	public static double umfang (double radius) {
		final double π = 3.1415926;	
		double u = 2 * radius * π;
		return u;
	}
	//Methode für den Oberföche A der Kugeln die ein doubel ausbigt und einen übernimt
	public static double oberfläche (double radius) {
		final double π = 3.1415926;	
		double a = 4 * radius*radius * π;
		return a;
	}
	//Methode für den Volumen der Kugeln die ein doubel ausbigt und einen übernimt
	public static double volumen (double radius) {
		final double π = 3.1415926;	
		double v = 4/3 * radius*radius*radius * π;
		return v;
	}
	
}
