﻿import java.util.Scanner;
import java.util.Arrays;
import java.lang.Math;

class Fahrkartenautomat {
	public static void main(String[] args) {
		// Scanner tastatur = new Scanner(System.in);

		// double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		// double eingeworfeneMünze;
		// double rückgabebetrag;
		// int anzahlticket;
		Boolean weiterbestellen = true;
		while (weiterbestellen == true) {
			double fahrkartenbestellung = fahrkartenbestellungErfasung();

			// Geldeinwurf
			// -----------
			eingezahlterGesamtbetrag = geldeinwurf(fahrkartenbestellung);

			// Fahrscheinausgabe
			// -----------------

			fakrkartendruck();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			double jageld = rückgeld(eingezahlterGesamtbetrag, fahrkartenbestellung);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n");

		}
	}

	// Methode zu erfassung der Wünsche des Kunden
	public static double fahrkartenbestellungErfasung() {
		Scanner tastatur = new Scanner(System.in); // Scanner erstellt nicht schließen ist so nen Java ding
		// Variabeln Global sonst nach Schleife keine verwendung möglich (Java!!)
		int wahl = 0;
		double preis = 0;
		double gesamtpreis = 0;
		int anzahltickets = 0;
		// Array erzeugen für die Auswahl
		String[][][] fahrkartenauswahl = new String[10][10][10];
		// Array füllen mit den Möglichkeiten
		fahrkartenauswahl[0][0][0] = " 1";
		fahrkartenauswahl[0][1][0] = "Einzelfahrschein Berlin AB";
		fahrkartenauswahl[0][0][1] = " 2,90 €";

		fahrkartenauswahl[1][0][0] = " 2";
		fahrkartenauswahl[1][1][0] = "Einzelfahrschein Berlin BC";
		fahrkartenauswahl[1][0][1] = " 3,30 €";

		fahrkartenauswahl[2][0][0] = " 3";
		fahrkartenauswahl[2][1][0] = "Einzelfahrschein Berlin ABC";
		fahrkartenauswahl[2][0][1] = " 3,60 €";

		fahrkartenauswahl[3][0][0] = " 4";
		fahrkartenauswahl[3][1][0] = "Kurzstrecke";
		fahrkartenauswahl[3][0][1] = " 1,90 €";

		fahrkartenauswahl[4][0][0] = " 5";
		fahrkartenauswahl[4][1][0] = "Tageskarte Berlin AB";
		fahrkartenauswahl[4][0][1] = " 8,60 €";

		fahrkartenauswahl[5][0][0] = " 6";
		fahrkartenauswahl[5][1][0] = "Tageskarte Berlin BC";
		fahrkartenauswahl[5][0][1] = " 9,00 €";

		fahrkartenauswahl[6][0][0] = " 7";
		fahrkartenauswahl[6][1][0] = "Tageskarte Berlin ABC";
		fahrkartenauswahl[6][0][1] = " 9,60 €";

		fahrkartenauswahl[7][0][0] = " 8";
		fahrkartenauswahl[7][1][0] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkartenauswahl[7][0][1] = "23,50 €";

		fahrkartenauswahl[8][0][0] = " 9";
		fahrkartenauswahl[8][1][0] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkartenauswahl[8][0][1] = "24,30 €";

		fahrkartenauswahl[9][0][0] = "10";
		fahrkartenauswahl[9][1][0] = "Kleingruppen-Tageskarte Berlin ABC";
		fahrkartenauswahl[9][0][1] = "24,90 €";

		while (wahl != 11 && anzahltickets < 10) { // Schleife um sicher zugehen das nich 11 eingegeben wurde und das
													// die Frage min 1 mahl kommt so wie das nicht meh als 10 Tikekts
													// erworben wurden
			System.out.println("Welchen Fahrschein wünschen Sie zu bestellen?\n");
			System.out.println("Auswahlnummer\t\tBezeichner\t\tPreis");
			System.out.println("_______________________________________________________________");

			// Aufruf des Arryas
			System.out.println(fahrkartenauswahl[0][0][0] + "\t\t" + fahrkartenauswahl[0][1][0] + "\t\t"
					+ fahrkartenauswahl[0][0][1]);
			System.out.println(fahrkartenauswahl[1][0][0] + "\t\t" + fahrkartenauswahl[1][1][0] + "\t\t"
					+ fahrkartenauswahl[1][0][1]);
			System.out.println(fahrkartenauswahl[2][0][0] + "\t\t" + fahrkartenauswahl[2][1][0] + "\t\t"
					+ fahrkartenauswahl[2][0][1]);
			System.out.println(fahrkartenauswahl[3][0][0] + "\t\t" + fahrkartenauswahl[3][1][0] + "\t\t\t\t"
					+ fahrkartenauswahl[3][0][1]);
			System.out.println(fahrkartenauswahl[4][0][0] + "\t\t" + fahrkartenauswahl[4][1][0] + "\t\t\t"
					+ fahrkartenauswahl[4][0][1]);
			System.out.println(fahrkartenauswahl[5][0][0] + "\t\t" + fahrkartenauswahl[5][1][0] + "\t\t\t"
					+ fahrkartenauswahl[5][0][1]);
			System.out.println(fahrkartenauswahl[6][0][0] + "\t\t" + fahrkartenauswahl[6][1][0] + "\t\t\t"
					+ fahrkartenauswahl[6][0][1]);
			System.out.println(fahrkartenauswahl[7][0][0] + "\t\t" + fahrkartenauswahl[7][1][0] + "\t"
					+ fahrkartenauswahl[7][0][1]);
			System.out.println(fahrkartenauswahl[8][0][0] + "\t\t" + fahrkartenauswahl[8][1][0] + "\t"
					+ fahrkartenauswahl[8][0][1]);
			System.out.println(fahrkartenauswahl[9][0][0] + "\t\t" + fahrkartenauswahl[9][1][0] + "\t"
					+ fahrkartenauswahl[9][0][1]);
			System.out.println("--------------------------------------------------------------------");
			System.out.println("11\t\tBezahlen\t\t\t\t" + gesamtpreis+" €");
			wahl = tastatur.nextInt(); // Eingabe der Auswahl
			while ((wahl < 1 || wahl > 10) && wahl != 11) // Abfrage ob auch nur die Möglichkeiten gewählt wurden

			{
				System.out.println("Sie haben eine fahlsche Auswahl getroffen.\n");
				System.out.println("Auswahlnimmer\t\tBezeichner\t\tPreis");
				System.out.println("_______________________________________________________________");
				// Aufruf des Arryas
				System.out.println(fahrkartenauswahl[0][0][0] + "\t\t" + fahrkartenauswahl[0][1][0] + "\t\t"
						+ fahrkartenauswahl[0][0][1]);
				System.out.println(fahrkartenauswahl[1][0][0] + "\t\t" + fahrkartenauswahl[1][1][0] + "\t\t"
						+ fahrkartenauswahl[1][0][1]);
				System.out.println(fahrkartenauswahl[2][0][0] + "\t\t" + fahrkartenauswahl[2][1][0] + "\t\t"
						+ fahrkartenauswahl[2][0][1]);
				System.out.println(fahrkartenauswahl[3][0][0] + "\t\t" + fahrkartenauswahl[3][1][0] + "\t\t\t\t"
						+ fahrkartenauswahl[3][0][1]);
				System.out.println(fahrkartenauswahl[4][0][0] + "\t\t" + fahrkartenauswahl[4][1][0] + "\t\t\t"
						+ fahrkartenauswahl[4][0][1]);
				System.out.println(fahrkartenauswahl[5][0][0] + "\t\t" + fahrkartenauswahl[5][1][0] + "\t\t\t"
						+ fahrkartenauswahl[5][0][1]);
				System.out.println(fahrkartenauswahl[6][0][0] + "\t\t" + fahrkartenauswahl[6][1][0] + "\t\t\t"
						+ fahrkartenauswahl[6][0][1]);
				System.out.println(fahrkartenauswahl[7][0][0] + "\t\t" + fahrkartenauswahl[7][1][0] + "\t"
						+ fahrkartenauswahl[7][0][1]);
				System.out.println(fahrkartenauswahl[8][0][0] + "\t\t" + fahrkartenauswahl[8][1][0] + "\t"
						+ fahrkartenauswahl[8][0][1]);
				System.out.println(fahrkartenauswahl[9][0][0] + "\t\t" + fahrkartenauswahl[9][1][0] + "\t"
						+ fahrkartenauswahl[9][0][1]);
				System.out.println("--------------------------------------------------------------------");
				System.out.println("11\t\tBezahlen\t\t\t\t" + gesamtpreis+" €");
				wahl = tastatur.nextInt(); // Erneute eingabe der Auswahl
			}
			if ((wahl == 1 || wahl == 2 || wahl == 3 || wahl == 4 || wahl == 5 || wahl == 6 || wahl == 7 || wahl == 8
					|| wahl == 9 || wahl == 10) && wahl != 11) // Ticket Anzahl bestellen nur für die Möglichkeiten
			{
				System.out.println("Sie können nur bis zu 10 Tickets erwerben.");
				System.out.printf("Sie haben bereits %d Tickets bestellt.\n ", anzahltickets);
				System.out.println("Wie viele Tickets möchten Sie?: ");
				anzahltickets = tastatur.nextInt(); // Eingabe der Anzahl der Tickets
				while (anzahltickets < 1 || anzahltickets > 10) { // Abfrage ob nicht zuviele Bestellt wurden oder
																	// negativ Ticket

					System.out.println("Sie haben was falsches eingegeben bitte geben sie eine neue Anzahl ein:");
					anzahltickets = tastatur.nextInt(); // Erneute eingabe der Ticketanzahl
				}
			}

			switch (wahl) { // Preis Berchnung
			case 1: { // Für fall 1 (geht auch mit if) hab Case gewählt um es auszubrobieren
				preis = +2.90;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 2: {
				preis = +3.30;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 3: {
				preis = +3.60;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 4: {
				preis = +1.90;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 5: {
				preis = +8.60;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 6: {
				preis = +9.00;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 7: {
				preis = +9.60;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 8: {
				preis = +23.50;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 9: {
				preis = +24.30;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 10: {
				preis = +24.90;
				gesamtpreis = gesamtpreis + (anzahltickets * preis);
				break;
			}
			case 11: { // Damit der Bezahl vorgang gestatet werden kann und der Gesamtpreis sicher
						// übernocmmen wird
				gesamtpreis = gesamtpreis;
				break;
			}
			default: {
				System.out.println("");
				break;
			}
			}
		}
		return gesamtpreis;
	}

	// Methode eingewurfene Münzen
	public static double geldeinwurf(double fahrkartenbestellung) {
		Scanner tastatur = new Scanner(System.in); // Scanner einfügen
		// Variablen Global für die Methode
		double eingezahlterGesamtbetrag = 0.0;
		// Abgleich ob schohn genügen eingewurfen wurde um die Tickests zubezahlen
		while (eingezahlterGesamtbetrag < fahrkartenbestellung) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (fahrkartenbestellung - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 100 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			if (eingeworfeneMünze == 0.05 || eingeworfeneMünze == 0.10 || eingeworfeneMünze == 0.50
					|| eingeworfeneMünze == 1 || eingeworfeneMünze == 2 || eingeworfeneMünze == 10
					|| eingeworfeneMünze == 5 || eingeworfeneMünze == 20 || eingeworfeneMünze == 50
					|| eingeworfeneMünze == 100) {
				eingezahlterGesamtbetrag += eingeworfeneMünze;
			} else {
				System.out.println("Einwurf ungültig");
			}
		}

		return eingezahlterGesamtbetrag;
	}

	// Methode zum Fahrscheindruck
	public static void fakrkartendruck() {

		System.out.println("\nFahrschein wird ausgegeben");
		// for (int i = 0; i < 8; i++)
		// {
		int millisekunden = 250;
		warte(millisekunden);
		// }
		System.out.println("\n\n");
	}

	// Metode für die Warte Zeit
	public static void warte(int millisekunden) {

		// System.out.print("=");
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Methode zu berechnung des Rückgeldes
	public static double rückgeld(double eingezahlterGesamtbetrag, double fahrkartenbestellung) {
		// Variable zu bestimmungdes Rückgeldes
		double rückgabebetrag;
		rückgabebetrag = eingezahlterGesamtbetrag - fahrkartenbestellung;

		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n ", (rückgabebetrag));
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 10.0) // 10 EURO-Schein
			{
				int betrag = 10;
				String einheit = " Euro";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag -= 10;
			}
			while (rückgabebetrag >= 5.0) // 5 EURO-Schein
			{
				int betrag = 5;
				String einheit = " Euro";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag -= 5;
			}
			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				int betrag = 2;
				String einheit = " Euro";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag -= 2;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				int betrag = 1;
				String einheit = " Euro";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag -= 1;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				int betrag = 50;
				String einheit = " Cent";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				int betrag = 20;
				String einheit = " Cent";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				int betrag = 10;
				String einheit = " Cent";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				int betrag = 5;
				String einheit = " Cent";
				muenzeAusgeben(betrag, einheit);
				rückgabebetrag -= 0.05;
			}
			// Wahrtezeit
			int millisekunden = 2500;
			warte(millisekunden);

		}
		return rückgabebetrag;
	}

	// Metode zur Ausgabe des Rückgeldes über Funktion zur besseren Optik
	static public void muenzeAusgeben(int betrag, String einheit) {

		if (betrag < 5.0 || einheit == " Cent") { // if betrag<5
			// System.out.println(" * * *");
			// System.out.println(" *\t *");
			// System.out.printf( "* %5d *\n",betrag);
			// System.out.println( "* "+ einheit+" *");
			// System.out.println(" * \t *");
			// System.out.println(" ***");

			System.out.printf("    * * *\n *\t   *\n* %5d     *\n* " + einheit + "     *\n * \t   *\n    * * *\n",
					betrag);

		} // ende bertag<5
		if (betrag >= 5.0 && einheit == " Euro") {
//		  System.out.println("* * * * * * * * * *");
//		  System.out.println("* \t\t  *");
//		  System.out.printf( "* %4d"+ einheit+"\t  *\n",betrag);
//		  System.out.println("* \t\t  *");
//		  System.out.println("* * * * * * * * * *");

			System.out.printf(
					"* * * * * * * * * *\n* \t\t  *\n* %4d " + einheit + "\t  *\n* \t\t  *\n* * * * * * * * * *\n",
					betrag);

		}

	}
}
