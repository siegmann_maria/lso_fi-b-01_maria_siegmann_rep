import java.util.ArrayList;


public class Schiffe {
	 	private String name;
	    private int energieversorgung;
	    private int schilde;
	    private int lebenserhaltung;
	    private int h�lle;
	    private int photonentorpedos;
	    private int reperaturandroiden;
	    public static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	    private int[] ladungsliste;
	    
	    
	    public Schiffe() {
	    	
	    }
	    
	    public Schiffe(int energieversorgung,int schilde,int lebenserhaltung,int h�lle,int photonentorpedos, int reperaturandroiden, ArrayList<String> broadcastKommunikator ,int[] ladungsliste) {
	    	this.energieversorgung = energieversorgung;
	    	this.schilde =schilde;
	    	this.lebenserhaltung=lebenserhaltung;
	    	this.h�lle=h�lle;
	    	this.photonentorpedos=photonentorpedos;
	    	this.reperaturandroiden=reperaturandroiden;
	    	this.broadcastKommunikator=broadcastKommunikator;
	    	this.ladungsliste=ladungsliste;
	    }
	    
	    
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getEnergieversorgung() {
			return energieversorgung;
		}
		public void setEnergieversorgung(int energieversorgung) {
			this.energieversorgung = energieversorgung;
		}
		public int getSchilde() {
			return schilde;
		}
		public void setSchilde(int schilde) {
			this.schilde = schilde;
		}
		public int getLebenserhaltung() {
			return lebenserhaltung;
		}
		public void setLebenserhaltung(int lebenserhaltung) {
			this.lebenserhaltung = lebenserhaltung;
		}
		public int getH�lle() {
			return h�lle;
		}
		public void setH�lle(int h�lle) {
			this.h�lle = h�lle;
		}
		public int getPhotonentorpedos() {
			return photonentorpedos;
		}
		public void setPhotonentorpedos(int photonentorpedos) {
			this.photonentorpedos = photonentorpedos;
		}
		public int getReperaturandroiden() {
			return reperaturandroiden;
		}
		public void setReperaturandroiden(int reperaturandroiden) {
			this.reperaturandroiden = reperaturandroiden;
		}
		public static ArrayList<String> getBroadcastKommunikator() {
			return broadcastKommunikator;
		}
		public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
			Schiffe.broadcastKommunikator = broadcastKommunikator;
		}
		public int[] getLadungsliste() {
			return ladungsliste;
		}
		public void setLadungsliste(int[] ladungsliste) {
			this.ladungsliste = ladungsliste;
		}
	
	

}
